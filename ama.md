# Ask me anything!

## [Ask a question](https://github.com/ShowMeBillyJo/ShowMeBillyJo/discussions/new?category=ama) | [Read questions](https://github.com/ShowMeBillyJo/ShowMeBillyJo/discussions/categories/ama)

Anything means *anything*. Personal questions. Money. Work. Life. Code. Whatever.

## Guidelines

* Ensure your question hasn't already been answered.
* Use a succinct title and description.
* Be civil and respectful.
* I reserve the right to delete any question for any reason.

## Links

* [Read more AMAs](https://github.com/sindresorhus/amas)
* [Create your own AMA](https://github.com/sindresorhus/amas/blob/main/create-ama.md)
* [What's an AMA?](https://en.wikipedia.org/wiki/R/IAmA)

## Credit

* [@sindresorhus](https://github.com/sindresorhus) for the overall concept
* [@obahareth](https://github.com/obahareth) for using discussions instead of issues
